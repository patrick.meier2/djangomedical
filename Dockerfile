FROM python:3
RUN apt-get update && apt-get upgrade -y && apt-get install -y dcmtk
ENV PYTHONUNBUFFERED 1
WORKDIR /code
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .