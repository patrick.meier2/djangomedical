from django.contrib import admin

from .models import Image
from .models import Tag

admin.site.register(Tag)
admin.site.register(Image)
