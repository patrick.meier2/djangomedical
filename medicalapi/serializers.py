from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework import serializers

from .models import Image
from .models import Tag


class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')


class ImageSerializer(WritableNestedModelSerializer):
    tags = TagSerializer(many=True)

    class Meta:
        model = Image
        fields = ('id', 'uri', 'description', 'comment', 'tags')
