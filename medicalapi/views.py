from rest_framework import viewsets

from .models import Image
from .models import Tag
from .serializers import ImageSerializer
from .serializers import TagSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all().order_by('uri')
    serializer_class = ImageSerializer

class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all().order_by('name')
    serializer_class = TagSerializer

