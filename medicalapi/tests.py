from django.test import TestCase

from .models import Image
from .models import Tag


class ImageTestCase(TestCase):
    def setUp(self):
        Image.objects.create(uri='123.1312', description='Test Description', comment='Test Comment')

    def test_image_can_be_accessed(self):
        image = Image.objects.get(uri='123.1312')
        self.assertEqual(image.description, 'Test Description', msg='Image accessed')

    def test_image_can_be_updated(self):
        image = Image.objects.get(uri='123.1312')
        image.comment = 'New Comment'
        image.save()
        self.assertEqual(Image.objects.get(uri='123.1312').comment, 'New Comment', msg='Image updated')

    def test_image_can_be_deleted(self):
        image = Image.objects.get(uri='123.1312')
        image.delete()
        self.assertEqual(Image.objects.all().count(), 0, msg='Image deleted')


class TagTestCase(TestCase):
    def setUp(self):
        Tag.objects.create(name='Karzinom')

    def test_tag_can_be_accessed(self):
        tag = Tag.objects.get(name='Karzinom')
        self.assertEqual(tag.name, 'Karzinom', msg='Tag accessed')

    def test_tag_can_be_updated(self):
        tag = Tag.objects.get(name='Karzinom')
        tag.name = 'New Name'
        tag.save()
        self.assertTrue(Tag.objects.get(name='New Name', msg='Tag updated'))

    def test_tag_can_be_deleted(self):
        tag = Tag.objects.get(name='Karzinom')
        tag.delete()
        self.assertEqual(Tag.objects.all().count(), 0, msg='Tag deleted')


class ImageTagTestCase(TestCase):
    t1 = i1 = None

    def setUp(self):
        self.t1 = Tag(name='Karzinom')
        self.i1 = Image(uri='123.1312', description='Test Description', comment='Test Comment')

    def test_tag_can_be_added_to_image(self):
        self.t1.save()
        self.i1.save()

        self.i1.tags.add(self.t1)

        self.assertTrue(Image.objects.filter(tags__name='Karzinom'), msg='Tag added')
