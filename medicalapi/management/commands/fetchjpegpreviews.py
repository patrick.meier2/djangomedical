import logging
import os

import cv2
import imutils as imutils
import requests
from django.core.management.base import BaseCommand

LOG_FILENAME = 'server_tasks.log'
logging.basicConfig(
    filename=LOG_FILENAME,
    level=logging.DEBUG,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)


class Command(BaseCommand):
    help = 'Fetch all images from the PACS server and generate JPEG previews in order to have faster loading times'

    def handle(self, *args, **options):
        base_url = 'http://orthanc:8042/instances/'
        instances = requests.get(base_url, auth=('orthanc', 'qTARAXigZk^#szPjfo8d')).json()
        added_items_count = 0

        new_path = r'files/fetched/'

        if not os.path.exists(new_path):
            os.makedirs(new_path)

        for instance in instances:
            uid = requests.get(f'{base_url}/{instance}', auth=('orthanc', 'qTARAXigZk^#szPjfo8d')).json()['MainDicomTags'][
                'SOPInstanceUID']

            with requests.get(f'{base_url}/{instance}/preview', auth=('orthanc', 'qTARAXigZk^#szPjfo8d'),
                              headers={'Accept': 'image/jpeg'}, stream=True) as r:
                with open(f'{new_path}{uid}.jpeg', 'wb') as f:
                    for chunk in r.iter_content(chunk_size=16 * 1024):
                        f.write(chunk)

                    added_items_count += 1

            # Resize the image
            image = cv2.imread(f'{new_path}{uid}.jpeg')
            resized_image = imutils.resize(image, width=256)
            cv2.imwrite(f'{new_path}{uid}.jpeg', resized_image)

        log_message = f"Successfully saved {added_items_count} Images"
        self.stdout.write(self.style.SUCCESS(log_message))
        logging.debug(log_message)
