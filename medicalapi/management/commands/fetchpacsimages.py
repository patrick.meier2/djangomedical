import json
import logging

import requests
from django.core.management.base import BaseCommand

LOG_FILENAME = 'server_tasks.log'
logging.basicConfig(
    filename=LOG_FILENAME,
    level=logging.DEBUG,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)


class Command(BaseCommand):
    help = 'Fetch all images and descriptions from the PACS server'
    django_url = 'http://django:8000/api/images'

    def handle(self, *args, **options):
        base_url = 'http://orthanc:8042/instances/'
        instances = requests.get(base_url, auth=('orthanc', 'qTARAXigZk^#szPjfo8d')).json()
        added_items_count = 0
        images = requests.get(self.django_url).json()
        pacs_uids = [i['uri'] for i in images]

        for instance in instances:
            description = None
            dicom_image = requests.get(f'{base_url}/{instance}', auth=('orthanc', 'qTARAXigZk^#szPjfo8d')).json()['MainDicomTags']

            if 'ImageComments' in dicom_image:
                description = dicom_image['ImageComments'].replace('^', ' ').replace(' ,', ',').rstrip().lstrip()

            uid = dicom_image['SOPInstanceUID']

            payload = json.dumps({'uri': uid, 'description': description})

            response = requests.post(self.django_url, data=payload)

            if response.status_code == 200:
                added_items_count += 1
                log_message = f"Added image with UID ${uid}"
                self.stdout.write(self.style.ERROR(log_message))
                logging.debug(log_message)
            else:
                log_message = response.text
                self.stdout.write(self.style.ERROR(log_message))
                logging.debug(log_message)

            if uid in pacs_uids:
                del pacs_uids[pacs_uids.index(uid)]

        if len(pacs_uids) > 0:
            for pacs_uid in pacs_uids:
                for i in images:
                    if i['uri'] == pacs_uid:
                        requests.delete(f"${self.django_url}/${i['id']}")

            log_message = f"Successfully deleted {len(pacs_uids)} images from Server"
            self.stdout.write(self.style.SUCCESS(log_message))
            logging.debug(log_message)

        if added_items_count > 0:
            log_message = f"Successfully imported {added_items_count} Images"
            self.stdout.write(self.style.SUCCESS(log_message))
            logging.debug(log_message)