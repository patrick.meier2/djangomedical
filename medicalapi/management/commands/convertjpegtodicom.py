import logging
import os
import subprocess
import xml.etree.ElementTree as ET

import requests
from django.core.management.base import BaseCommand

LOG_FILENAME = 'server_tasks.log'
logging.basicConfig(
    filename=LOG_FILENAME,
    level=logging.DEBUG,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)


def get_description_for_images():
    descriptions = {}

    root = ET.parse('files/images.xml').getroot()

    for child in root.findall('image'):
        image = child.attrib['src'][:-4]
        description = child.find('description').text.replace('\n', '')
        uid = child.find('uid').text

        descriptions[image] = {'description': description.replace(' ', '^').replace("'", ""), 'uid': uid}

    return descriptions


class Command(BaseCommand):
    help = 'Convert all the jpg files to dicom files with the description from images.xml'

    def upload_file(self, path):
        f = open(path, "rb")
        content = f.read()
        f.close()

        base_url = 'http://orthanc:8042/instances/'
        response = requests.post(base_url, auth=('orthanc', 'qTARAXigZk^#szPjfo8d'), data=content, headers={'content-type': 'application/dicom'})

        if response.status_code == 200:
            log_message = f"Uploaded image to orthanc"
            self.stdout.write(self.style.ERROR(log_message))
            logging.debug(log_message)
        else:
            log_message = response.text
            self.stdout.write(self.style.ERROR(log_message))
            logging.debug(log_message)


    def handle(self, *args, **options):
        images = get_description_for_images()
        counter = 0
        error_count = 0

        new_path = r'files/converted'

        if not os.path.exists(new_path):
            os.makedirs(new_path)

        for filename, values in images.items():
            try:
                subprocess.run(
                    f"img2dcm files/images/{filename}.jpg files/converted/{filename}.dcm -k 'Modality=OT' -k 'SOPInstanceUID=1.2.826.0.1.3680043.8.654.50.2010.{values['uid']}' -k 'SeriesNumber=1' -k 'Modality=OT' -k 'SeriesInstanceUID=1.2.826.0.1.3680043.8.654.20.2010' -k 'StudyInstanceUID=1.2.826.0.1.3680043.8.654.10.2010' -k 'StudyDate=20100303' -k 'ReferringPhysicianName=House^Gregory' -k 'PatientSex=M' -k 'PatientBirthDate=19700101' -k 'PatientID=424243' -k 'PatientName=Telvis^Test' -k 'StudyDescription=Duennschnittstudien' -k 'ImageComments={values['description']}' -k 'StudyID=0000100'",
                    shell=True, check=True)

                counter += 1

                log_message = f"Converting files/images/{filename}.jpg"
                self.stdout.write(self.style.SUCCESS(log_message))
                logging.debug(log_message)


                file = f'files/converted/{filename}.dcm'

                self.upload_file(file)
                os.remove(file)
            except subprocess.CalledProcessError:
                error_count += 1
                print(filename, values['description'], "Wasn't able to process.")

        log_message = f"Successfully converted {counter} Images"
        error_log_message = f"{error_count} images unsuccessful"
        self.stdout.write(self.style.SUCCESS(log_message))
        self.stdout.write(self.style.ERROR(error_log_message))
        logging.debug(log_message)
        logging.debug(error_log_message)