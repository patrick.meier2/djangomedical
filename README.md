# Application Server

## Project setup
### Runs django for development
```
python manage.py runserver
```

## Project configuration
### Access admin interface
```
http://127.0.0.1:8000/admin/
```
Login with admin:admin.

### Access API
```
http://127.0.0.1:8000/api/
```